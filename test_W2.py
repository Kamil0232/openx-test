import unittest
import OpenX_W2
import urllib.request

users = urllib.request.urlopen('https://jsonplaceholder.typicode.com/users')
users = eval(users.read())
posts = urllib.request.urlopen('https://jsonplaceholder.typicode.com/posts')
posts = eval(posts.read())


class TestVariant1(unittest.TestCase):
    def test_post_sum(self):

        for user_test in range(len(users)):
            self.assertEqual(OpenX_W2.post_summary(users[user_test]), 10)

    def test_distance(self):
        user1 = users[0]
        user2 = users[1]
        user3 = users[2]
        user4 = users[3]
        user5 = users[4]
        user6 = users[5]
        user7 = users[6]
        user8 = users[7]
        user9 = users[8]
        user10 = users[9]
        self.assertEqual(OpenX_W2.calc_distance(user1), user5)
        self.assertEqual(OpenX_W2.calc_distance(user2), user3)
        self.assertEqual(OpenX_W2.calc_distance(user3), user2)
        self.assertEqual(OpenX_W2.calc_distance(user4), user9)
        self.assertEqual(OpenX_W2.calc_distance(user5), user10)
        self.assertEqual(OpenX_W2.calc_distance(user6), user10)
        self.assertEqual(OpenX_W2.calc_distance(user7), user5)
        self.assertEqual(OpenX_W2.calc_distance(user8), user4)
        self.assertEqual(OpenX_W2.calc_distance(user9), user4)
        self.assertEqual(OpenX_W2.calc_distance(user10), user5)

    def test_titles(self):
        self.assertEqual(OpenX_W2.check_titles(posts), "All of the posts have unique titles")


if __name__ == '__main__':
    unittest.main()
