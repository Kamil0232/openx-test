import urllib.request
from geopy.distance import geodesic

users = urllib.request.urlopen('https://jsonplaceholder.typicode.com/users')
users = eval(users.read())
# The array of dictionaries for users
posts = urllib.request.urlopen('https://jsonplaceholder.typicode.com/posts')
posts = eval(posts.read())
# The array of dictionaries for posts


def post_summary(user):
# Calculate the number of the post that each user has written
    sum_post = 0
    for j in range(len(posts)):
        if user["id"] == posts[j]["userId"]:
            sum_post = sum_post + 1
    return sum_post


def check_titles(posts_list):
# Check if all of the titles are unique - if not, store the duplicate in the doubles array
    doubles = []
    for k in range(len(posts_list)):
        for z in range(len(posts_list)):
            if posts_list[k]["id"] != posts_list[z]["id"]:
                if posts_list[k]["title"] == posts_list[z]["title"]:
                    doubles.append(posts_list[z]["id"])
    if not doubles:
        return ("All of the posts have unique titles")

    if doubles:
        return doubles


def calc_distance(user):
# Find the nearest user for each user. Coordinates are assumed to be Earth's coordinates. 
    distance_max = 20038 #maximal distance on Earth surface
    nearest_user = user
    for k in range(len(users)):
        if user != users[k]:
            cor1 = (user["address"]["geo"]["lat"], user["address"]["geo"]["lng"])
            cor2 = (users[k]["address"]["geo"]["lat"], users[k]["address"]["geo"]["lng"])
            if geodesic(cor1, cor2).kilometers < distance_max:
                distance_max = geodesic(cor1, cor2).kilometers
                nearest_user = users[k]
    return nearest_user


def show_results():
# Print the nearest user for each user. Print the array of quantity of posts for every single user. Then, check if all the posts have unique titles. 
    list_of_posts = []
    for i in range(len(users)):
        list_of_posts.append('User {} wrote {} posts'.format(users[i]["username"], post_summary(users[i])))
        print('Nearest user for user {} is user {}'.format(users[i]["username"], calc_distance(users[i])["username"]))
    print(list_of_posts)
    print(check_titles(posts))
