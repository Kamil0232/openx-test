import statistics

# Each node was treated as a separate class with the value specified as self_value.
# Direct offspring are contained in the offspring array and total offspring in complete_offspring array.
# To determine the values of a given subtree, the value at its beginning is also used.


class Node:
    def __init__(self, self_value):
        self.self_value = self_value
        self.offspring = []
        self.complete_offspring = []
        self.average = 0
        # self.average - parameter to calculate the average of the self_values in subtree
        self.summary = 0
        # self.average - parameter to calculate the sum of the self_values in subtree
        self.medians = 0
        # self.average - parameter to calculate the median of the self_values in subtree

    def add_offspring(self, y_node):
    # Method to add the direct offspring relations to each Node
        self.offspring = self.offspring + y_node

    def find_all(self):
    # Method to find all the elements of subtree for each Node
        self.complete_offspring[:] = self.offspring[:]
        for i in range(len(self.offspring)):
            self.complete_offspring = self.complete_offspring + self.offspring[i].find_all()
        return self.complete_offspring

    def calc_values(self):
    # Method to calculate sum, average and median for each Node
        self.find_all()
        self_values_list = []
        for i in range(len(self.complete_offspring)):
            self_values_list.append(self.complete_offspring[i].self_value)
        self_values_list.append(self.self_value)
        self.summary = sum(self_values_list)
        self.average = statistics.mean(self_values_list)
        self.medians = statistics.median(self_values_list)







